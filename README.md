# OpenLP Live Display

View OpenLP in the browser just as it would appear on the projector!

#### How to use

You need to use OpenLP version 2.9.2 or higher as this requires the theme API.

Put all the files into this folder within the OpenLP data folder: `stages/live/` (you may have to create these folders)

Now run OpenLP and open a browser at the url: `<openlp_ip>:<openlp_port>/stage/live`

#### Compatability

This stage view can show:
 - Songs
 - Bibles
 - Images
 - PDFs
 - Custom slides

But it cannot show:

 - Videos (including video backgrounds)
 - Presentations (that use Microsoft office or open office)
 - The desktop
