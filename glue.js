host = window.location.hostname;
websocket_port = 4317;
api_URL = `http://${host}:4316/api/v2`;

screenshotMode = false;
savedScreenshot = null;
currentItem = '';
activeSlide = 0;
currentTheme = null;
ws = null;

setTimeout(()=>{
    Display.init({
        isDisplay: true,
        doItemTransitions: true
    });
    document.body.style.background = '#000';
    target_thing = document.getElementsByClassName('reveal')[0];
    target_thing.style.transformOrigin = 'top';

    ws = new WebSocket(`ws://${host}:${websocket_port}`);
    ws.onmessage = (event) => {
        const reader = new FileReader();
        reader.onload = () => {
            const state = JSON.parse(reader.result.toString()).results;
            activeSlide = state.slide;
            if (state.item != currentItem) {
                currentItem = state.item;
                fullUpdate();
            } else {
                if (screenshotMode) {
                    sendScreenShotToScreen();
                } else {
                    Display.goToSlide(activeSlide);
                }
            }
        };
        reader.readAsText(event.data);
    };
}, 100);

window.onresize = (event) => {
    if (currentTheme != null) {
        updateThemeScale();
    }
}

function fullUpdate() {
    getTheme()
    .then(theme_raw => {
        return theme_raw.json();
    })
    .then(theme => {
        sendThemeToScreen(theme);
        return getLiveItem();
    })
    .then(live_item => {
        return live_item.json();
    })
    .then(sendLiveItemToScreen);
}

function sendLiveItemToScreen(service_item) {
    if (service_item.capabilities.includes(10) || service_item.name == 'images') {
        // includes ItemCapabilities.ProvidesOwnDisplay
        screenshotMode = true;
        sendScreenShotToScreen();
    } else {
        screenshotMode = false;
        savedScreenshot = null;
        service_item.slides.map((value, index) => {
            value.tag = index;
        });
        Display.setTextSlides(service_item.slides);
        Display.goToSlide(activeSlide);
    }
}

function sendScreenShotToScreen() {
    getScreenShot()
    .then(data => data.json())
    .then(screenshot => {
        if (screenshot.binary_image == savedScreenshot) return;
        savedScreenshot = screenshot.binary_image;
        Display.setFullscreenImage('#000', screenshot.binary_image);
    })
}

function sendThemeToScreen(theme) {
    // Catch any non supported theme background types
    switch (theme.background_type) {
        case 'transparent':
        case 'solid':
        case 'gradient':
        case 'image':
            break;
        default:
            theme.background_type = 'solid';
            theme.background_color = '#000';
    }

    currentTheme = theme;
    updateThemeScale();
    Display.setTheme(theme);
}


function getLiveItem() {
    return fetch(`${api_URL}/controller/live-items`);
}

function getScreenShot() {
    return fetch(`${api_URL}/core/live-image`);
}

function getTheme() {
    return fetch(`${api_URL}/controller/live-theme`);
}

function updateThemeScale() {
    target_thing = document.getElementsByClassName('reveal')[0];
    scale_by_x = document.body.clientWidth / currentTheme.display_size_width;
    scale_by_y = document.body.clientHeight / currentTheme.display_size_height;
    scale_by = Math.min(scale_by_x, scale_by_y);
    target_thing.style.transform = `scale(${scale_by})`;
    target_thing.style.width = `${currentTheme.display_size_width}px`;
    target_thing.style.height = `${currentTheme.display_size_height}px`;
}
